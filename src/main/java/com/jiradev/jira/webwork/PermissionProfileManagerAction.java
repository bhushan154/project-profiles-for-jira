package com.jiradev.jira.webwork;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jiradev.ao.PermissionProfile;
import com.jiradev.api.PermissionProfileService;
import com.jiradev.api.ProjectPermissionMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import javax.inject.Inject;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class PermissionProfileManagerAction extends JiraWebActionSupport
{
    @Inject
    public PermissionProfileManagerAction(PermissionProfileService permissionProfileService){
        this.permissionProfileService = permissionProfileService;
    }

    @ComponentImport
    private final PermissionProfileService permissionProfileService;
    private static final Logger log = LoggerFactory.getLogger(PermissionProfileManagerAction.class);
    private int profileId;
    private List<Project> projects = new ArrayList<Project>();
    private List<PermissionProfile> profiles = new ArrayList<PermissionProfile>();
    private ProjectManager projectManager = ComponentAccessor.getProjectManager();
    private PermissionSchemeService permissionSchemeService =
            ComponentAccessor.getComponentOfType(PermissionSchemeService.class);
    private ApplicationUser loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();

    private Collection<ProjectPermissionMapping> permissionMappings;
    private PermissionProfile permissionProfile;


    public String doDefault() throws Exception {
        setProjects(projectManager.getProjects());
        setProfiles(permissionProfileService.all());
        return "input";
    }

    public String doExecute() throws Exception {
        setProjects(projectManager.getProjects());
        return "input";
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<PermissionProfile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<PermissionProfile> profiles) {
        this.profiles = profiles;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public Collection<ProjectPermissionMapping> getPermissionMappings() {
        return permissionMappings;
    }

    public void setPermissionMappings(Collection<ProjectPermissionMapping> permissionMappings) {
        this.permissionMappings = permissionMappings;
    }

    public PermissionProfile getPermissionProfile() {
        return permissionProfile;
    }

    public void setPermissionProfile(PermissionProfile permissionProfile) {
        this.permissionProfile = permissionProfile;
    }
}
