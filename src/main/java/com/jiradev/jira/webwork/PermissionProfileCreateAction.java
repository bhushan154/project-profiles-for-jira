package com.jiradev.jira.webwork;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.gson.Gson;
import com.jiradev.ao.PermissionProfile;
import com.jiradev.api.PermissionProfileService;
import com.jiradev.api.ProjectPermissionMapping;
import com.jiradev.service.PermissionProfileGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

public class PermissionProfileCreateAction extends JiraWebActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(PermissionProfileCreateAction.class);

    @Inject
    public PermissionProfileCreateAction(PermissionProfileService permissionProfileService){
        this.permissionProfileService = permissionProfileService;
    }

    @ComponentImport
    private final PermissionProfileService permissionProfileService;
    private ProjectManager projectManager = ComponentAccessor.getProjectManager();
    private PermissionSchemeService permissionSchemeService =
            ComponentAccessor.getComponentOfType(PermissionSchemeService.class);
    private ApplicationUser loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    private String name;
    private Collection<ProjectPermissionMapping> permissionMappings;
    private PermissionProfile permissionProfile;


    public String doDefault() throws Exception {
        return "input";
    }

    public String doCreate() throws Exception {
        try {
            PermissionProfileGenerator generator = new PermissionProfileGenerator(projectManager, permissionSchemeService, permissionProfileService, loggedInUser);
            setPermissionProfile(generator.createProfile(getName()));
            return "success";
        }
        catch (Exception exc) {
            addErrorMessage(exc.getMessage());
            return "error";
        }
    }


    // Getter and setter

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PermissionProfile getPermissionProfile() {
        return permissionProfile;
    }

    public void setPermissionProfile(PermissionProfile permissionProfile) {
        this.permissionProfile = permissionProfile;
    }

}
