package com.jiradev.jira.webwork;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.security.IssueSecurityLevelScheme;
import com.atlassian.jira.issue.security.IssueSecuritySchemeService;
import com.atlassian.jira.notification.NotificationScheme;
import com.atlassian.jira.notification.NotificationSchemeService;
import com.atlassian.jira.permission.PermissionScheme;
import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jiradev.ao.PermissionProfile;
import com.jiradev.api.PermissionProfileService;
import com.jiradev.api.ProjectPermissionMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import javax.inject.Inject;
import java.lang.reflect.Type;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Collection;

public class PermissionProfileApplyAction extends JiraWebActionSupport
{
    @Inject
    public PermissionProfileApplyAction(PermissionProfileService permissionProfileService) {
        this.permissionProfileService = permissionProfileService;
    }

    private static final Logger log = LoggerFactory.getLogger(PermissionProfileApplyAction.class);

    @ComponentImport
    private final PermissionProfileService permissionProfileService;
    private int profileId;
    private ApplicationUser loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    private PermissionProfile permissionProfile;
    private ProjectManager projectManager = ComponentAccessor.getProjectManager();
    private PermissionSchemeService permissionSchemeService = ComponentAccessor.getComponentOfType(PermissionSchemeService.class);
    private NotificationSchemeService notificationSchemeService = ComponentAccessor.getComponentOfType(NotificationSchemeService.class);
    private IssueSecuritySchemeService issueSecuritySchemeService = ComponentAccessor.getComponentOfType(IssueSecuritySchemeService.class);
    private ProjectService projectService = ComponentAccessor.getComponentOfType(ProjectService.class);
    Collection<ProjectPermissionMapping> permissionMappings;


    public String doConfirm() throws Exception {
        setPermissionProfile(permissionProfileService.getPermissionProfile(getProfileId()));
        setPermissionMappings(getPermissionMappingsFromProfile(permissionProfile));
        for(ProjectPermissionMapping mapping:permissionMappings) {
            Project project = projectManager.getProjectObj(mapping.getProjectId());
            PermissionScheme permissionScheme = permissionSchemeService.getPermissionScheme(loggedInUser, mapping.getSchemeId()).get();
            if(project == null && permissionScheme != null) {
                addErrorMessage("Project with ID " + mapping.getProjectId() + " could not be found. Skipping update.");
            }
            else if(project != null && permissionScheme == null) {
                addErrorMessage("Project " + project.getName() + " (" + project.getKey() + ") will not be updated since permission scheme with ID " + mapping.getSchemeId() + " could not be found. Skipping update.");
            }
            else if(project == null && permissionScheme == null){
                addErrorMessage("Project with ID " + mapping.getProjectId() + " and permission scheme with ID " + mapping.getSchemeId() + " could not be found. Skipping update.");
            }
        }
        return "confirm"; //returns SUCCESS
    }

    public String doApply() throws Exception {
        setPermissionProfile(permissionProfileService.getPermissionProfile(getProfileId()));
        setPermissionMappings(getPermissionMappingsFromProfile(permissionProfile));
        for(ProjectPermissionMapping mapping:permissionMappings){
            Project project = projectManager.getProjectObj(mapping.getProjectId());
            PermissionScheme permissionScheme = permissionSchemeService.getPermissionScheme(loggedInUser, mapping.getSchemeId()).get();
            NotificationScheme notificationScheme = notificationSchemeService.getNotificationSchemeForProject(loggedInUser, mapping.getProjectId()).get();
            IssueSecurityLevelScheme issueSecurityLevelScheme = issueSecuritySchemeService.getIssueSecurityLevelSchemeForProject(loggedInUser, mapping.getProjectId()).get();
            Long notificationSchemeId = null;
            if(notificationScheme != null)
                notificationSchemeId = notificationScheme.getId();
            Long issueSecurityLevelSchemeId = null;
            if(issueSecurityLevelScheme != null)
                issueSecurityLevelSchemeId = issueSecurityLevelScheme.getId();
            if(project != null && permissionScheme != null){
                ProjectService.UpdateProjectSchemesValidationResult validationResult = projectService.validateUpdateProjectSchemes(loggedInUser,
                        permissionScheme.getId(),
                        notificationSchemeId,
                        issueSecurityLevelSchemeId);
                if(validationResult.isValid()){
                    projectService.updateProjectSchemes(validationResult, project);
                }
                else {
                    addErrorCollection(validationResult.getErrorCollection());
                    return "error";
                }
            }
        }
        return "success"; //returns SUCCESS
    }


    // Getter and setters

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public PermissionProfile getPermissionProfile() {
        return permissionProfile;
    }

    public void setPermissionProfile(PermissionProfile permissionProfile) {
        this.permissionProfile = permissionProfile;
    }

    private  Collection<ProjectPermissionMapping>getPermissionMappingsFromProfile(PermissionProfile permissionProfile){
        Gson gson = new Gson();
        Type collectionType = new TypeToken<Collection<ProjectPermissionMapping>>(){}.getType();
        permissionMappings = new ArrayList<ProjectPermissionMapping>();
        permissionMappings = gson.fromJson(permissionProfile.getMapping(), collectionType);
        return permissionMappings;
    }

    public Collection<ProjectPermissionMapping> getPermissionMappings() {
        return permissionMappings;
    }

    public void setPermissionMappings(Collection<ProjectPermissionMapping> permissionMappings) {
        this.permissionMappings = permissionMappings;
    }

    public PermissionSchemeService getPermissionSchemeService() {
        return permissionSchemeService;
    }

    public void setPermissionSchemeService(PermissionSchemeService permissionSchemeService) {
        this.permissionSchemeService = permissionSchemeService;
    }
}
