package com.jiradev.jira.webwork;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.jiradev.ao.PermissionProfile;
import com.jiradev.api.PermissionProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class PermissionProfileDeleteAction extends JiraWebActionSupport
{

    public PermissionProfileDeleteAction(PermissionProfileService permissionProfileService) {
        this.permissionProfileService = permissionProfileService;
    }

    private static final Logger log = LoggerFactory.getLogger(PermissionProfileDeleteAction.class);

    @ComponentImport
    private final PermissionProfileService permissionProfileService;
    private int profileId;



    public String doDefault() throws Exception {
        PermissionProfile permissionProfile = permissionProfileService.getPermissionProfile(getProfileId());
        return "input";
    }

    public String doDelete() throws Exception {
        try {
            PermissionProfile permissionProfile = permissionProfileService.getPermissionProfile(getProfileId());
            permissionProfileService.deletePermissionProfile(permissionProfile);
            return "success";
        }
        catch(Exception exc) {
            return "error";
        }
    }


    // Getter and setter
    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }
}
