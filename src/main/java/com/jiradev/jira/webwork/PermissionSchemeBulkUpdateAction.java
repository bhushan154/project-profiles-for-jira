package com.jiradev.jira.webwork;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.security.IssueSecurityLevelScheme;
import com.atlassian.jira.issue.security.IssueSecuritySchemeService;
import com.atlassian.jira.notification.NotificationScheme;
import com.atlassian.jira.notification.NotificationSchemeService;
import com.atlassian.jira.permission.PermissionScheme;
import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.jiradev.ao.PermissionProfile;
import com.jiradev.api.PermissionProfileService;
import com.jiradev.service.PermissionProfileGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class PermissionSchemeBulkUpdateAction extends JiraWebActionSupport
{
    @Inject
    public PermissionSchemeBulkUpdateAction(PermissionProfileService permissionProfileService) {
        this.permissionProfileService = permissionProfileService;
    }

    @ComponentImport
    private final PermissionProfileService permissionProfileService;
    private static final Logger log = LoggerFactory.getLogger(PermissionSchemeBulkUpdateAction.class);
    private ApplicationUser loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    private ProjectManager projectManager = ComponentAccessor.getProjectManager();
    private PermissionSchemeService permissionSchemeService = ComponentAccessor.getComponentOfType(PermissionSchemeService.class);
    private NotificationSchemeService notificationSchemeService = ComponentAccessor.getComponentOfType(NotificationSchemeService.class);
    private IssueSecuritySchemeService issueSecuritySchemeService = ComponentAccessor.getComponentOfType(IssueSecuritySchemeService.class);
    private Collection<PermissionScheme> permissionSchemes = new ArrayList<PermissionScheme>();
    private Collection<Project> projects = new ArrayList<Project>();
    private String[] projectToUpdate;
    private ProjectService projectService = ComponentAccessor.getComponentOfType(ProjectService.class);
    private int permissionScheme;
    PermissionProfile permissionProfile;


    public String doDefault() throws Exception {
        setProjects(projectManager.getProjectObjects());
        setPermissionSchemes(permissionSchemeService.getPermissionSchemes(loggedInUser).get());
        return "input";
    }

    public String doUpdate() throws Exception {
        PermissionProfileGenerator generator = new PermissionProfileGenerator(projectManager, permissionSchemeService, permissionProfileService, loggedInUser);
        setPermissionProfile(generator.createProfile(null));
        try {
            for (String projectId : getProjectToUpdate()) {
                Project project = projectManager.getProjectObj(new Long(projectId));
                PermissionScheme permissionScheme = permissionSchemeService.getPermissionScheme(loggedInUser, new Long(getPermissionScheme())).get();
                NotificationScheme notificationScheme = notificationSchemeService.getNotificationSchemeForProject(loggedInUser, project.getId()).get();
                IssueSecurityLevelScheme issueSecurityLevelScheme = issueSecuritySchemeService.getIssueSecurityLevelSchemeForProject(loggedInUser, project.getId()).get();
                Long notificationSchemeId = null;
                if (notificationScheme != null)
                    notificationSchemeId = notificationScheme.getId();
                Long issueSecurityLevelSchemeId = null;
                if (issueSecurityLevelScheme != null)
                    issueSecurityLevelSchemeId = issueSecurityLevelScheme.getId();
                if (project != null && permissionScheme != null) {
                    ProjectService.UpdateProjectSchemesValidationResult validationResult = projectService.validateUpdateProjectSchemes(loggedInUser,
                            permissionScheme.getId(),
                            notificationSchemeId,
                            issueSecurityLevelSchemeId);
                    if (validationResult.isValid()) {
                        projectService.updateProjectSchemes(validationResult, project);
                    } else {
                        addErrorCollection(validationResult.getErrorCollection());
                        return "error";
                    }
                }
            }
        }
        catch(Exception exc){
            addErrorMessage(exc.getMessage());
            return "error";
        }
        return "success"; //returns SUCCESS
    }


    public Collection<PermissionScheme> getPermissionSchemes() {
        return permissionSchemes;
    }

    public void setPermissionSchemes(Collection<PermissionScheme> permissionSchemes) {
        this.permissionSchemes = permissionSchemes;
    }

    public Collection<Project> getProjects() {
        return projects;
    }

    public void setProjects(Collection<Project> projects) {
        this.projects = projects;
    }

    public String[] getProjectToUpdate() {
        return projectToUpdate;
    }

    public void setProjectToUpdate(String[] projectToUpdate) {
        this.projectToUpdate = projectToUpdate;
    }

    public int getPermissionScheme() {
        return permissionScheme;
    }

    public void setPermissionScheme(int permissionScheme) {
        this.permissionScheme = permissionScheme;
    }

    public PermissionProfile getPermissionProfile() {
        return permissionProfile;
    }

    public void setPermissionProfile(PermissionProfile permissionProfile) {
        this.permissionProfile = permissionProfile;
    }
}
