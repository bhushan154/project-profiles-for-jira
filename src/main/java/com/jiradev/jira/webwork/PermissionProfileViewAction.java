package com.jiradev.jira.webwork;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.permission.PermissionScheme;
import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jiradev.ao.PermissionProfile;
import com.jiradev.api.PermissionProfileService;
import com.jiradev.api.ProjectPermissionMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import javax.inject.Inject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

public class PermissionProfileViewAction extends JiraWebActionSupport
{
    @Inject
    public PermissionProfileViewAction(PermissionProfileService permissionProfileService) {
        this.permissionProfileService = permissionProfileService;

    }

    //Logger
    private static final Logger log = LoggerFactory.getLogger(PermissionProfileViewAction.class);

    @ComponentImport
    private final PermissionProfileService permissionProfileService;

    // Variables
    private int profileId;
    private ApplicationUser loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    private Collection<ProjectPermissionMapping> permissionMappings;
    private ProjectManager projectManager = ComponentAccessor.getProjectManager();
    private PermissionSchemeService permissionSchemeService = ComponentAccessor.getComponentOfType(PermissionSchemeService.class);
    private PermissionProfile permissionProfile;

    public String doDefault() throws Exception {
        try {
            setPermissionProfile(permissionProfileService.getPermissionProfile(getProfileId()));
            Type collectionType = new TypeToken<Collection<ProjectPermissionMapping>>(){}.getType();
            Gson gson = new Gson();
            permissionMappings = new ArrayList<ProjectPermissionMapping>();
            permissionMappings = gson.fromJson(permissionProfile.getMapping(), collectionType);
            addErrorCollection(validateErrors(permissionMappings));
            return "input";
        }
        catch(Exception exc) {
            return "error";
        }
    }

    public String doApply() throws Exception {
        try {

            return "success";
        }
        catch(Exception exc) {
            return "error";
        }
    }

    private ErrorCollection validateErrors(Collection<ProjectPermissionMapping> permissionMappings) {
        ErrorCollection errorCollection = new SimpleErrorCollection();
        for(ProjectPermissionMapping mapping: permissionMappings) {
            Project project = projectManager.getProjectObj(mapping.getProjectId());
            PermissionScheme permissionScheme = permissionSchemeService.getPermissionScheme(loggedInUser, mapping.getSchemeId()).get();
            if(project == null){
                errorCollection.addErrorMessage("Project with ID " + mapping.getProjectId() + " not found or no longer exists.");
            }
            if(permissionScheme == null){
                errorCollection.addErrorMessage("Permission Scheme with ID " + mapping.getSchemeId() + " not found or no longer exists.");
            }
        }
        return errorCollection;
    }

    // Getter and Setter

    public String doExecute() throws Exception {

        return super.execute(); //returns SUCCESS
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    @Override
    public ApplicationUser getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(ApplicationUser loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public Collection<ProjectPermissionMapping> getPermissionMappings() {
        return permissionMappings;
    }

    public PermissionProfile getPermissionProfile() {
        return permissionProfile;
    }

    public void setPermissionProfile(PermissionProfile permissionProfile) {
        this.permissionProfile = permissionProfile;
    }

    @Override
    public ProjectManager getProjectManager() {
        return projectManager;
    }

    public PermissionSchemeService getPermissionSchemeService() {
        return permissionSchemeService;
    }
}
