package com.jiradev.service;

import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.gson.Gson;
import com.jiradev.ao.PermissionProfile;
import com.jiradev.api.PermissionProfileService;
import com.jiradev.api.ProjectPermissionMapping;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class PermissionProfileGenerator {

    public PermissionProfileGenerator(ProjectManager projectManager,
                                      PermissionSchemeService permissionSchemeService,
                                      PermissionProfileService permissionProfileService,
                                      ApplicationUser loggedInUser){
        this.projectManager = projectManager;
        this.permissionSchemeService = permissionSchemeService;
        this.permissionProfileService = permissionProfileService;
        this.loggedInUser = loggedInUser;
    }

    private ProjectManager projectManager;
    private PermissionSchemeService permissionSchemeService;
    private PermissionProfileService permissionProfileService;
    private ApplicationUser loggedInUser;

    public PermissionProfile createProfile(String name) throws Exception{
        Collection<ProjectPermissionMapping> permissionMappings = permissionMappings = new ArrayList<ProjectPermissionMapping>();
        for (Project proj : projectManager.getProjects()) {
            permissionMappings.add(new ProjectPermissionMapping(proj.getId(), permissionSchemeService.getSchemeAssignedToProject(loggedInUser, proj.getId()).get().getId()));
        }
        Gson gson = new Gson();
        String permissionMappingJson = gson.toJson(permissionMappings);
        // Check if name entered is not empty then set name to entered name
        if(name == null || name.length() == 0)
            name = generateProfileName();
        return permissionProfileService.add(name, permissionMappingJson);
    }

    private String generateProfileName(){
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyMMddhhmmssMs");
        String datetime = ft.format(dNow);
        String name = datetime + "-Permission_Profile";
        return name;
    }
}
