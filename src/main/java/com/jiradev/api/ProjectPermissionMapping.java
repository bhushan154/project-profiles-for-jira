package com.jiradev.api;

public class ProjectPermissionMapping {

    public ProjectPermissionMapping(Long projectId, Long schemeId){
        this.projectId = projectId;
        this.schemeId = schemeId;
    }

    private long projectId;
    private Long schemeId;


    public Long getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(Long schemeId) {
        this.schemeId = schemeId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }
}
