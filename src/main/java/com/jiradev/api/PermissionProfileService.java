package com.jiradev.api;

import com.atlassian.activeobjects.tx.Transactional;
import com.jiradev.ao.PermissionProfile;

import java.sql.SQLException;
import java.util.List;


@Transactional
public interface PermissionProfileService {

    PermissionProfile add(String name, String mapping) throws SQLException;

    List<PermissionProfile> all() throws SQLException;

    PermissionProfile getPermissionProfile(int permissionProfileId)  throws SQLException;

    void deletePermissionProfile(PermissionProfile permissionProfile)  throws SQLException;
}
