package com.jiradev.impl;
import com.atlassian.activeobjects.external.ActiveObjects;

import java.util.Date;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.jiradev.ao.PermissionProfile;
import com.jiradev.api.PermissionProfileService;

import javax.inject.Inject;
import javax.inject.Named;

@Scanned
@Named
public class PermissionProfileServiceImpl implements PermissionProfileService {

    @ComponentImport
    private final ActiveObjects ao;

    @Inject
    public PermissionProfileServiceImpl(ActiveObjects ao)
    {
        this.ao = checkNotNull(ao);
    }

    public PermissionProfile add(String name, String jsonMapping)
    {
        final PermissionProfile permissionProfile = ao.create(PermissionProfile.class);
        permissionProfile.setDateCreated(new Date().getTime());
        permissionProfile.setName(name);
        permissionProfile.setMapping(jsonMapping);
        permissionProfile.save();
        return permissionProfile;
    }

    public List<PermissionProfile> all()
    {
        return newArrayList(ao.find(PermissionProfile.class));
    }

    public PermissionProfile getPermissionProfile(int permissionProfileId){
        return ao.get(PermissionProfile.class, permissionProfileId);
    }

    public void deletePermissionProfile(PermissionProfile permissionProfile){
        ao.delete(permissionProfile);
    }
}
