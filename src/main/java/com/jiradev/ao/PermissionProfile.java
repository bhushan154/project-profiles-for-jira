package com.jiradev.ao;
import net.java.ao.Entity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public interface PermissionProfile extends Entity {

    // new Date().getTime() of when it was created
    Long getDateCreated();
    void setDateCreated(Long dateCreated);

    @StringLength(value= StringLength.UNLIMITED)
    String getMapping();
    void setMapping(String json);

    String getName();
    void setName(String name);
}

